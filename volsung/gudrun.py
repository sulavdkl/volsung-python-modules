#!/usr/bin/env python3

"""

gudrun.py

Base class library to work with the Gudrun wellbore simulator

"""

import subprocess
import os
import sys
import shutil
import tempfile
import h5py
import psutil

class WellheadProfileData(object):
    def __init__(self):
        """
        Class representing wellhead profile data in a Gudrun simulation.
        """
        self.clear()
        
    def clear(self):
        """
        Clears the current data.
        """
        self.data = {}.copy()
    
    def loadHDF(self, hdf):
        """
        Load data from hdf file.
        """
        self.clear()
        for n in hdf['/results/wellboremodel/wellheadprofile']:
            self.data[str(n)] = hdf['/results/wellboremodel/wellheadprofile/%s' % n][:]
            
class WellboreProfileData(object):
    def __init__(self):
        """
        Class representing wellbore profile data in a Gudrun simulation.
        """
        self.clear()
        
    def clear(self):
        """
        Clears the current data.
        """
        self.data = {}.copy()
    
    def loadHDF(self, hdf):
        """
        Load data from hdf file.
        """
        self.clear()
        for n in hdf['/results/wellboremodel/wellboreprofile']:
            self.data[str(n)] = hdf['/results/wellboremodel/wellboreprofile/%s' % n][:]

class FeedzoneCollectionData(object):
    def __init__(self):
        """
        Class representing feedzone collection data in a Gudrun simulation.
        """
        self.clear()
        
    def clear(self):
        """
        Clears the current data.
        """
        self.data = {}.copy()
    
    def loadHDF(self, hdf):
        """
        Load data from hdf file.
        """
        self.clear()
        for n in hdf['/results/wellboremodel/feedzonecollection']:
            self.data[str(n)] = hdf['/results/wellboremodel/feedzonecollection/%s' % n][:]

class WellboreSimulationData(object):
    def __init__(self):
        """
        Class representing wellbore simulation (text log) data in a Gudrun simulation.
        """
        self.clear()
        
    def clear(self):
        """
        Clears the current data.
        """
        self.Log = ''
    
    def loadHDF(self, hdf):
        """
        Load wellhead data from hdf file.
        """
        self.clear()
        self.Log = (hdf['/results/wellboremodel/wellboresimulation/Log'][0]).decode('utf-8')

class Gudrun(object):
    def __init__(self):
        """
        Class to run Gudrun models and process their output
        """
        self.clear()

    def clear(self):
        """
        Clears the current data
        """
        self.__gudrunOutput = ''
        self.WellheadProfileData = WellheadProfileData()
        self.WellboreProfileData = WellboreProfileData()
        self.FeedzoneCollectionData = FeedzoneCollectionData()
        self.WellboreSimulationData = WellboreSimulationData()
        
    def __startGudrunProcess(self, args):
        """
        Attempts to start the Gudrun process and returns it.
        Raises an exception if Gudrun can't be started.        
        """
        # create a list of candidates for running Gudrun
        candidates = []
        
        # is the VOLSUNGPATH environment variable set? if so then use that location for signy
        vpath = ''
        try:
            vpath = os.environ['VOLSUNGPATH']
            if sys.platform == 'linux' or sys.platform == 'darwin':
                candidates.append(vpath + '/bin/gudrun')
                candidates.append(vpath + '/gudrun')
            else:
                candidates.append(vpath + '/bin/gudrun.exe')
                candidates.append(vpath + '/gudrun.exe')
        except:
            pass
        # linux: add more default candidates
        if sys.platform == 'linux':
            candidates += ['volsung.gudrun', 'gudrun']
        
        # now iterate through the candidates and return the process if we're able to start it
        for c in candidates:
            if shutil.which(c) is None:
                continue
            cmd = '"' + c + '" ' + ' '.join(args)           # use '"' to ensure white space in path are not a problem
            # catch the output in a temporary file which we attach to the subprocess
            temp_stdout = tempfile.TemporaryFile()
            p = subprocess.Popen(cmd, shell=[False if sys.platform == 'win32' else True][0], stdout=temp_stdout, stderr=subprocess.STDOUT)
            p.temp_stdout = temp_stdout
            return p
        raise FileNotFoundError("Could not launch gudrun; is the VOLSUNGPATH environmental variable set correctly? Current value is '%s'." % vpath)
        return None
    
    def run(self, fname, outfile = None):
        """
        Run Gudrun over a single model given by the file fname.
        If outfile is not specified then a temporary file will be used.
        Once finished will load the results from outfile.
        Returns the exit code from Gudrun.
        """
        self.clear()
        
        tempf = None
        if outfile is None:
            # Note:
            # A complication when making a temporary file is that SNAP does not allow
            # creating files in the /tmp folder
            # Hence create the temporary file in the same location as fname
            tempf = tempfile.NamedTemporaryFile(prefix = os.path.dirname(os.path.abspath(fname)) + "/")
            outfile = tempf.name
            tempf.close()

        # create the command line arguments
        args = []
        args.append('--verbose')
        args.append('--command')
        args.append('-o')
        args.append('"%s"' % outfile)
        args.append('"%s"' % fname)
        
        # start the running process
        p = self.__startGudrunProcess(args)

        # collate the output from the process        
        while p.poll() is None:
            pass
        p.temp_stdout.seek(0)
        self.__gudrunOutput = p.temp_stdout.read().decode("utf-8")
        p.temp_stdout = None            # this will delete the temporary file
            
        # all ok?
        if p.returncode >= 0:
            self.loadOutput(outfile)
            if tempf is not None:
                os.remove(outfile)
        return p.returncode
    
    def __pollProcesses(self, processes, nprocesses):
        """
        Auxiliary: poll processes to ensure that no more than nprocesses are running.
        Once a process has stopped remove it from the list.
        Returns the number of current processes.
        """
        while True:
            for i in range(len(processes)):
                if processes[i].poll() is not None:
                    # process has finished, remove it from list
                    processes.remove(processes[i])
                    break
            if len(processes) < nprocesses:
                return len(processes)
        return len(processes)
    
    def batchRun(self, infiles, outfiles, nprocesses = psutil.cpu_count()):
        """
        Run the model files given in the infiles list in parallel, using up to nprocesses at the same time.
        The model outputs will be written to the file names given in the outfiles list.
        """
        self.clear()
        
        processes = []
        for i in range(len(infiles)):
            # create the command line arguments
            args = []
            args.append('--verbose')
            args.append('--command')
            args.append('-o')
            args.append('"%s"' % outfiles[i])
            args.append('"%s"' % infiles[i])
            # wait here until a slot for the processes is available
            self.__pollProcesses(processes, nprocesses)
            # start the new process
            processes.append(self.__startGudrunProcess(args))            
        # pass time until the remaining processes have finished
        while self.__pollProcesses(processes, nprocesses) != 0:
            pass

    def output(self):
        """
        Returns the output from the last call to Signy.
        This can be helpful for debugging errors.
        """
        return self.__gudrunOutput

    def loadOutput(self, fname):
        """
        Load data from the hdf5 output file fname after a simulation has run.
        """
        hdf = h5py.File(fname, 'r')
        self.WellheadProfileData.loadHDF(hdf)
        self.WellboreProfileData.loadHDF(hdf)        
        self.FeedzoneCollectionData.loadHDF(hdf)
        self.WellboreSimulationData.loadHDF(hdf)
        hdf.close()