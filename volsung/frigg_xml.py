#!/usr/bin/env python3

"""

frigg_xml.py

Library to provide xml related classes from Frigg

"""

from volsung.helpers_xml import *

class RelativePermeability(object):
    def __init__(self):
        """
        Abstract base class for relative permeabilities.
        Subclasses need to at least implement className()
        """
        self.clear()
        
    def clear(self):
        pass
    
    def addNode(self, parent):
        """
        Return xml node with own data
        """
        return addNode(parent, 'relativepermeability', attr = {'class': self.className()})

class RP_None(RelativePermeability):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "RP_None"
    
class RP_Corey(RelativePermeability):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "RP_Corey"
    
    def clear(self):
        self.SL_residual = 0.05
        self.SG_residual = 0.30
        
    def addNode(self, parent):
        node = super().addNode(parent)
        addNode(node, 'slresidual', self.SL_residual)
        addNode(node, 'sgresidual', self.SG_residual)
        return node
    
class RP_Grant(RP_Corey):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "RP_Grant"
    
class RP_FattKlikoff(RelativePermeability):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "RP_FattKlikoff"
    
    def clear(self):
        self.SL_residual = 0.05
        
    def addNode(self, parent):
        node = super().addNode(parent)
        addNode(node, 'slresidual', self.SL_residual)
        return node

class RP_Linear(RelativePermeability):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "RP_Linear"
    
    def clear(self):
        self.SG_lower = 0.0
        self.SG_upper = 1.0
        self.SL_lower = 0.0
        self.SL_upper = 1.0
        
    def addNode(self, parent):
        node = super().addNode(parent)
        addNode(node, 'sglower', self.SG_lower)
        addNode(node, 'sgupper', self.SG_upper)
        addNode(node, 'sllower', self.SL_lower)
        addNode(node, 'slupper', self.SL_upper)
        return node

class RP_PerfectlyMobile(RelativePermeability):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "RP_PerfectlyMobile"
    
class RP_Pickens(RelativePermeability):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "RP_Pickens"
    
    def clear(self):
        self.KL_slope = 1.0
        
    def addNode(self, parent):
        node = super().addNode(parent)
        addNode(node, 'klslope', self.KL_slope)
        return node

class RP_Richards(RelativePermeability):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "RP_Richards"
    
    def clear(self):
        self.KL_B = 11.0175;
        self.KL_C = 10000.0;
        self.KL_Q = 4.51762e+07;
        self.KL_v = 0.613287;
        self.KG_B = 25.0030;
        self.KG_C = 9999.95;
        self.KG_Q = 1.69975e+08;
        self.KG_v = 100.000;
        
    def addNode(self, parent):
        node = super().addNode(parent)
        addNode(node, 'klb', self.KL_B)
        addNode(node, 'klc', self.KL_C)
        addNode(node, 'klq', self.KL_Q)
        addNode(node, 'klv', self.KL_v)
        addNode(node, 'kgb', self.KG_B)
        addNode(node, 'kgc', self.KG_C)
        addNode(node, 'kgq', self.KG_Q)
        addNode(node, 'kgv', self.KG_v)
        return node
        
class RP_VanGenuchtenMualem(RelativePermeability):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "RP_VanGenuchtenMualem"
    
    def clear(self):
        self.Lambda = 1.0
        self.SL_residual = 0.05
        self.SL_saturated = 0.3
        self.SG_residual = 0.35
        
    def addNode(self, parent):
        node = super().addNode(parent)
        addNode(node, 'lambda', self.Lambda)
        addNode(node, 'slresidual', self.SL_residual)
        addNode(node, 'slsaturated', self.SL_saturated)
        addNode(node, 'sgresidual', self.SG_residual)
        return node    
    
class RP_Verma(RelativePermeability):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "RP_Verma"
    
    def clear(self):
        self.SL_residual = 0.2
        self.SL_saturated = 0.895
        self.A = 1.259
        self.B = -1.7615
        self.C = 0.5089
        
    def addNode(self, parent):
        node = super().addNode(parent)
        addNode(node, 'slresidual', self.SL_residual)
        addNode(node, 'slsaturated', self.SL_saturated)
        addNode(node, 'a', self.A)
        addNode(node, 'b', self.B)
        addNode(node, 'c', self.C)
        return node    
    
    