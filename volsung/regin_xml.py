#!/usr/bin/env python3

"""

regin_xml.py

Library to build Regin wellbore models programmatically

Not implemented yet:
    - use of aqueous tracers
    
"""

import numpy
import copy
import enum
from xml.etree import ElementTree

from volsung.signy import *
from volsung.helpers_xml import *
from volsung.frigg_xml import *
from volsung.yggdrasil_xml import *

#
# Classes required by Regin
#

class InterpolationType(enum.Enum):
    """
    Interpolation type for Reservoir1D
    """
    Step         = 0
    Linear       = 1

class Reservoir1D(object):
    def __init__(self, thermotable):
        """
        Class describing 1-D reservoir thermodynamic properties along wellbore.
        """
        self.ThermoTable = thermotable
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.data = []
        self.RelativePermeability = RP_None()
        self.InterpolationType = InterpolationType.Step
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'reservoir'

    def add(self, z, state, t = 0.0):
        """
        Add thermodynamic state to data
        """
        state.constrain()
        self.data.append((t, z, state))
        
    def addNode(self, parent):
        """
        Return xml node with own data
        """
        n = len(self.data)
        node = addNode(parent, self.tag(), attr = {'class': 'Reservoir1D'})
        addNode(node, 'interpolationtype', self.InterpolationType.value)
        for i in range(n):
            r = addNode(node, 'row')
            addNode(r, 't', self.data[i][0])
            addNode(r, 'z', self.data[i][1])
            addNode(r, 'p', self.data[i][2].Pressure)
            addNode(r, 'h', self.data[i][2].Enthalpy)
            for k in self.data[i][2].MassFraction.keys():
                addNode(r, ('X' + k).lower(), self.data[i][2].MassFraction[k])
        self.RelativePermeability.addNode(node)
        return node        
    
    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        for n in node.iter('row'):
            t = nodeFloat(n, 't')
            z = nodeFloat(n, 'z')
            p = nodeFloat(n, 'p')
            h = nodeFloat(n, 'h')
            X = {}
            for k in self.ThermoTable.components():
                X[k] = nodeFloat(n, ('X' + k).lower())
            self.add(z, ThermoState(self.ThermoTable, p, h, X), t = t)

class WellTrack(object):
    def __init__(self):
        """
        Class describing a welltrack using XYZ and measured depth MD
        """
        self.clear()
        
    def calculateMD(self):
        """
        From XYZ calculate the MD coordinate
        """
        n = self.X.size
        self.MD = numpy.array([0.0] * n, dtype = numpy.float64)
        for i in reversed(range(n-1)):
            dx = self.X[i+1] - self.X[i]
            dy = self.Y[i+1] - self.Y[i]
            dz = self.Z[i+1] - self.Z[i]
            dL = (dx*dx + dy*dy + dz*dz)**0.5
            self.MD[i] = self.MD[i-1] + dL
            
    def clear(self):
        """
        Clears the current data.
        """
        self.X  = numpy.array([], dtype = numpy.float64)
        self.Y  = numpy.array([], dtype = numpy.float64)
        self.Z  = numpy.array([], dtype = numpy.float64)
        self.MD = numpy.array([], dtype = numpy.float64)
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'welltrack'
        
    def setXYZ(self, x, y, z):
        """
        Set the new (x,y,z) coordinates of the track.
        They will get sorted so that last index contains wellhead (i.e. maximum elevation) coordinates
        """
        index = numpy.argsort(copy.copy(z))
        self.X = numpy.array(x, dtype = numpy.float64)[index]
        self.Y = numpy.array(y, dtype = numpy.float64)[index]
        self.Z = numpy.array(z, dtype = numpy.float64)[index]
        self.calculateMD()
        
    def measuredDepth(self, z):
        """
        Returns the linearly interpolated measured depth for given elevation z.
        """
        return numpy.interp(z, self.Z, self.MD)
        
    def elevation(self, md):
        """
        Returns the elevation for given measured depth md.
        """
        return numpy.interp(md, numpy.flip(self.MD), numpy.flip(self.Z))

    def addNode(self, parent):
        """
        Return xml node with own data
        """
        n = self.X.size
        node = addNode(parent, self.tag(), attr = {'class': 'WellTrack', 'npoints': str(n)})
        for i in range(n):
            p = addNode(node, 'point', attr = {'index': str(i)})
            addNode(p, 'v', self.X[i], {'row': '0'})
            addNode(p, 'v', self.Y[i], {'row': '1'})
            addNode(p, 'v', self.Z[i], {'row': '2'})
        return node
    
    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        n = int(node.attrib['npoints'])
        x = numpy.array([float('nan')] * n, dtype = numpy.float64)
        y = numpy.array([float('nan')] * n, dtype = numpy.float64)
        z = numpy.array([float('nan')] * n, dtype = numpy.float64)
        pt = [x,y,z]
        for p in node.iter('point'):
            index = int(p.attrib['index'])
            for v in p.iter('v'):
                row = int(v.attrib['row'])
                pt[row][index] = float(v.text)
        self.setXYZ(x, y, z)
        
class FlowPathSegmentModifier(object):
    def __init__(self):
        """
        Abstract modifier class for flow path segments
        Sublasses must implement className() method
        """
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'flowpathsegmentmodifier'
        
    def addNode(self, parent):
        """
        Default class implementation
        """
        return addNode(parent, self.tag(), attr = {'class': self.className()})

    def parse(self, node):
        pass
    
class FPSM_Constant(FlowPathSegmentModifier):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return 'FPSM_Constant'
        
class FPSM_Chronological(FlowPathSegmentModifier):
    def __init__(self):
        """
        FlowPathSegment modifier using ID/OD/rugosity from a chronological table.
        """
        super().__init__()
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.table = {}.copy()
        
    def className(self):
        return 'FPSM_Chronological'
    
    def add(self, t, ID, OD, rugosity):
        """
        Add a parameter set entry to the table
        
        t               : time in seconds since 1/1/1970
        id              : inner diameter [m]
        od              : outer diameter [m]
        rugosity        : rugosity [m]
        """
        self.table[t] = {}.copy()
        self.table[t]['id'] = ID
        self.table[t]['od'] = OD
        self.table[t]['rugosity'] = rugosity
        
    def addNode(self, parent):
        node = super().addNode(parent)
        t = addNode(node, 'parametertable')
        for time in self.table.keys():
            r = addNode(t, 'flowpathsegmentmodifierparameters')
            r.attrib['time'] = str(time)
            for k in self.table[time].keys():
                addNode(r, k, float(self.table[time][k]))
        return node
    
    def parse(self, node):
        self.clear()
        t = node.find('parametertable')
        for v in t.iter('flowpathsegmentmodifierparameters'):
            t = float(v.attrib['time'])
            self.table[t] = {}.copy()
            for n in v.iter():
                try:
                    self.table[t][n.tag] = float(n.text)
                except TypeError:
                    pass

def newFlowPathSegmentModifier(class_t):
    """
    Object factory for creating FlowPathSegment modifier of given class_t
    """
    if class_t == 'FPSM_Constant':
        return FPSM_Constant()
    elif class_t == 'FPSM_Chronological':
        return FPSM_Chronological()
    raise KeyError("[newFlowPathSegmentModifier]: Unknown class type '%s'." % class_t)
        
class SegmentType(enum.Enum):
    """
    Enumerator for FlowPath segment types; same values as internally defined in Volsung's Regin project
    """
    BoreHole    = 0
    Casing      = 1
    Cement      = 2
    SlottedLiner= 3
    Pipe        = 4
    Sleeve      = 5
    Fish        = 6
    Scale       = 7
    Insulation  = 8
    
class FlowPathSegment(object):
    def __init__(self, top_md = 0.0, bottom_md = 0.0, id = 0.0, od = 0.0, rugosity = 0.0, segtype=SegmentType.SlottedLiner, starttime=-1.7976931348623157e+308, endtime=1.7976931348623157e+308, description='', color=(255,255,255), fps_modifier = FPSM_Constant()):
        """
        Convenience constructor
        
        top_md      : measured depth [m] of top of segment
        bottom_md   : measured dpeht [m] of bottom of segment
        id          : segment inner diameter [m]
        od          : segment outer diameter [m]
        rugosity    : segment rugosity [m]
        segtype     : type of segment, see SegmentType enumerator
        starttime   : begin of active period for segment; default is always active
        endtime     : end of active period for segment; default is always active
        description : optional text description
        color       : RGB colour information
        fps_modifier: Modifier for the flow path segment
        
        Note:
            You can use the welltrack's conversion methods for determining top_md, bottom_md from elevations
        """
        self.TopMD = top_md
        self.BottomMD = bottom_md
        self.ID = id
        self.OD = od
        self.Rugosity = rugosity
        self.Type = segtype
        self.StartTime = starttime
        self.EndTime = endtime
        self.Description = description
        self.Color = color
        self.Modifier = fps_modifier
        
    def clear(self):
        """
        Clears the current data
        """
        self.TopMD = 0.0
        self.BottomMD = 1.0
        self.ID = 0.25
        self.OD = 0.27
        self.Rugosity = 0.0
        self.Type = SegmentType.SlottedLiner
        self.StartTime = -1.7976931348623157e+308
        self.EndTime = 1.7976931348623157e+308
        self.Description = ''
        self.Color = (255,255,255)
        self.Modifier = FPSM_Constant()
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'flowpathsegment'

    def addNode(self, parent):
        """
        Return xml node with own data
        """
        node = addNode(parent, self.tag(), attr = {'class': 'FlowPathSegment'})
        addNode(node, 'topmd', self.TopMD)
        addNode(node, 'bottommd', self.BottomMD)
        addNode(node, 'id', self.ID)
        addNode(node, 'od', self.OD)
        addNode(node, 'rugosity', self.Rugosity)
        addNode(node, 'segmenttype', self.Type.value)
        addNode(node, 'starttime', self.StartTime)
        addNode(node, 'endtime', self.EndTime)
        addNode(node, 'description', self.Description)
        addColorNode(node, 'color', self.Color)
        self.Modifier.addNode(node)
        return node
    
    def parse(self, node):
        """
        Parse object from node
        """
        self.TopMD = nodeFloat(node, 'topmd')
        self.BottomMD = nodeFloat(node, 'bottommd')
        self.ID = nodeFloat(node, 'id')
        self.OD = nodeFloat(node, 'od')
        self.Rugosity = nodeFloat(node, 'rugosity')
        self.Type = SegmentType(nodeInt(node, 'segmenttype'))
        self.StartTime = nodeFloat(node, 'starttime')
        self.EndTime = nodeFloat(node, 'endtime')
        self.Description = nodeString(node, 'description')
        self.Color = nodeColor(node, 'color')
        try:
            self.Modifier = newFlowPathSegmentModifier(node.find(FlowPathSegmentModifier.tag()).attrib['class'])
            self.Modifier.parse(node.find(self.Modifier.tag()))    
        except AttributeError:
            # versions < 1.18.0 don't have modifiers, attrib[''] fails
            self.Modifier = FPSM_Constant()
        
class FlowPath(object):
    def __init__(self):
        """
        The flow path configuration for the wellbore model.
        """
        # create the public structure
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.Segments = []
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'flowpath'
        
    def add(self, segment):
        """
        Add flow path segment to collection
        """
        self.Segments.append(segment)

    def addNode(self, parent):
        """
        Return xml node with own data
        """
        node = addNode(parent, self.tag(), attr = {'class': 'FlowPath'})
        for s in self.Segments:
            s.addNode(node)
        return node
    
    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        for s in node.iter(FlowPathSegment.tag()):
            seg = FlowPathSegment()
            seg.parse(s)
            self.add(seg)

class FlowingTimeType(enum.Enum):
    """
    Enumerator for HeatLossParameters flowing time type; same values as internally defined in Volsung's Regin project
    """
    DifferenceToCommission  = 0
    Absolute                = 1

class HeatLossParameters(object):
    def __init__(self):
        """
        The heatloss parameters
        """
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.Enabled = False
        self.IsothermalDetection = True
        self.FlowingTimeType = FlowingTimeType.DifferenceToCommission
        self.AbsoluteFlowingTime = 0.0
        self.FormationDensity = 2600.0
        self.FormationSpecificHeat = 1000.0
        self.FormationThermalConductivity = 2.0
        self.CementThermalConductivity = 0.29
        self.SteelThermalConductivity = 43.0
        self.InsulationThermalConductivity = 0.022

    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'heatlossparameters'

    def addNode(self, parent):
        """
        Return xml node with own data
        """
        node = addNode(parent, self.tag(), attr = {'class': 'HeatLossParameters'})
        addNode(node, 'enabled', self.Enabled)        
        addNode(node, 'isothermaldetection', self.IsothermalDetection)
        addNode(node, 'flowingtimetype', self.FlowingTimeType.value)
        addNode(node, 'absoluteflowingtime', self.AbsoluteFlowingTime)
        addNode(node, 'formationdensity', self.FormationDensity)
        addNode(node, 'formationspecificheat', self.FormationSpecificHeat)
        addNode(node, 'formationthermalconductivity', self.FormationThermalConductivity)
        addNode(node, 'cementthermalconductivity', self.CementThermalConductivity)
        addNode(node, 'steelthermalconductivity', self.SteelThermalConductivity)
        addNode(node, 'insulationthermalconductivity', self.InsulationThermalConductivity)
        return node    

    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        self.Enabled = nodeBool(node, 'enabled')
        self.IsothermalDetection = nodeBool(node, 'isothermaldetection')
        self.FlowingTimeType = FlowingTimeType(nodeInt(node, 'flowingtimetype'))
        self.AbsoluteFlowingTime = nodeFloat(node, 'absoluteflowingtime')
        self.FormationDensity = nodeFloat(node, 'formationdensity')
        self.FormationSpecificHeat = nodeFloat(node, 'formationspecificheat')
        self.FormationThermalConductivity = nodeFloat(node, 'formationthermalconductivity')
        self.CementThermalConductivity = nodeFloat(node, 'cementthermalconductivity')
        self.SteelThermalConductivity = nodeFloat(node, 'steelthermalconductivity')
        self.InsulationThermalConductivity = nodeFloat(node, 'insulationthermalconductivity')
        
class WellboreProfileFieldData(object):
    def __init__(self):
        """
        Data structure describing flowing survey field data along wellbore.
        """
        # create public structure
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.TimeFilterOn = False
        self.TimeFilterInterval = 2592000.0
        self.Time = numpy.array([], dtype = numpy.float64)
        self.Elevation = numpy.array([], dtype = numpy.float64)
        self.Pressure = numpy.array([], dtype = numpy.float64)
        self.Temperature = numpy.array([], dtype = numpy.float64)
        self.GasMassFraction = numpy.array([], dtype = numpy.float64)
        self.Velocity = numpy.array([], dtype = numpy.float64)
        self.VelocityLiquid = numpy.array([], dtype = numpy.float64)
        self.VelocityGas = numpy.array([], dtype = numpy.float64)
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'wellboreprofilefielddata'
        
    def setData(self, time = [], elevation = [], pressure = [], temperature = [], gas_mass_fraction = [], velocity = [], velocity_liquid = [], velocity_gas = []):
        """
        Set the field data.
        Data will be internally converted to numpy arrays.
        The size of the arrays will be altered to match the longest array by padding with NANs
        """
        # convenience for the user:
        # stretch all data arrays to the same length
        # length is given by the longest data array
        # fill all other values with NANs
        N = max(len(time), len(elevation), len(pressure), len(temperature), len(gas_mass_fraction), len(velocity), len(velocity_liquid), len(velocity_gas))
        self.Time = numpy.array([float('nan')] * N)
        self.Elevation = numpy.array([float('nan')] * N)
        self.Pressure = numpy.array([float('nan')] * N)
        self.Temperature = numpy.array([float('nan')] * N)
        self.GasMassFraction = numpy.array([float('nan')] * N)
        self.Velocity = numpy.array([float('nan')] * N)
        self.VelocityLiquid = numpy.array([float('nan')] * N)
        self.VelocityGas = numpy.array([float('nan')] * N)

        self.Time[0:len(time)] = numpy.array(time)
        self.Elevation[0:len(elevation)] = numpy.array(elevation)
        self.Pressure[0:len(pressure)] = numpy.array(pressure)
        self.Temperature[0:len(temperature)] = numpy.array(temperature)
        self.GasMassFraction[0:len(gas_mass_fraction)] = numpy.array(gas_mass_fraction)
        self.Velocity[0:len(velocity)] = numpy.array(velocity)
        self.VelocityLiquid[0:len(velocity_liquid)] = numpy.array(velocity_liquid)
        self.VelocityGas[0:len(velocity_gas)] = numpy.array(velocity_gas)
        
    def addNode(self, parent):
        """
        Return xml node with own data
        """
        node = addNode(parent, self.tag(), attr = {'class': 'WellboreProfileFieldData'})
        addNode(node, 'timefilteron', self.TimeFilterOn)
        addNode(node, 'timefilterinterval', self.TimeFilterInterval)
        addNode(node, 'time', self.Time)
        addNode(node, 'elevation', self.Elevation)
        addNode(node, 'pressure', self.Pressure)        
        addNode(node, 'temperature', self.Temperature)
        addNode(node, 'gasmassfraction', self.GasMassFraction)
        addNode(node, 'velocity', self.Velocity)
        addNode(node, 'velocityliquid', self.VelocityLiquid)
        addNode(node, 'velocitygas', self.VelocityGas)
        return node    

    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        self.TimeFilterOn = nodeBool(node, 'timefilteron')
        self.TimeFilterInterval = nodeFloat(node, 'timefilterinterval')
        t = nodeFloatVector(node, 'time')
        z = nodeFloatVector(node, 'elevation')
        p = nodeFloatVector(node, 'pressure')
        T = nodeFloatVector(node, 'temperature')
        x = nodeFloatVector(node, 'gasmassfraction')
        v = nodeFloatVector(node, 'velocity')
        vliq = nodeFloatVector(node, 'velocityliquid')
        vgas = nodeFloatVector(node, 'velocitygas')
        self.setData(time = t, elevation = z, pressure = p, temperature = T, gas_mass_fraction = x, velocity = v, velocity_liquid = vliq, velocity_gas = vgas)

class WellboreProfile(object):
    def __init__(self):
        """
        Data relating to the wellbore profile generation.
        """
        # create the public structure
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.DeltaZ = 10.0          # default spacing between discrete elevations in the profile
        self.ExtraElevations = []   # list of additional elevations for the discrete profile
        self.IgnoreRatHole = True   # ignore rat hole - i.e. no profile generated below deepest feedzone
        self.FieldData = WellboreProfileFieldData()
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'wellboreprofile'

    def addNode(self, parent):
        """
        Return xml node with own data
        """
        node = addNode(parent, self.tag(), attr = {'class': 'WellboreProfile'})
        addNode(node, 'deltaz', self.DeltaZ)
        addNode(node, 'extraelevations', self.ExtraElevations)
        addNode(node, 'ignorerathole', self.IgnoreRatHole)
        self.FieldData.addNode(node)
        return node    
        
    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        self.DeltaZ = nodeFloat(node, 'deltaz')
        self.ExtraElevations = nodeFloatList(node, 'extraelevations')
        self.IgnoreRatHole = nodeBool(node, 'ignorerathole')
        self.FieldData.parse(node.find(self.FieldData.tag()))
    
class WellheadProfileFieldData(object):
    def __init__(self, thermotable):
        """
        Data structure describing flowing survey field data at the wellhead
        """
        self.ThermoTable = thermotable
        # create public structure
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.TimeFilterOn = False
        self.TimeFilterInterval = 2592000.0
        self.Time = numpy.array([], dtype = numpy.float64)
        self.Pressure = numpy.array([], dtype = numpy.float64)
        self.Enthalpy = numpy.array([], dtype = numpy.float64)
        self.MassRate = numpy.array([], dtype = numpy.float64)
        self.MassFraction = {}.copy()
        for k in self.ThermoTable.components():
            self.MassFraction[k] = numpy.array([], dtype = numpy.float64)
            
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'wellheadprofilefielddata'
        
    def setData(self, time = [], pressure = [], enthalpy = [], mass_rate = [], mass_fraction = {}):
        """
        Set the field data.
        Data will be internally converted to numpy arrays.
        The size of the arrays will be altered to match the longest array by padding with NANs
        """
        # convenience for the user:
        # stretch all data arrays to the same length
        # length is given by the longest data array
        # fill all other values with NANs
        N = max(len(time), len(pressure), len(enthalpy), len(mass_rate))
        for k in mass_fraction.keys():
            N = max(N, len(mass_fraction[k]))
        self.Time = numpy.array([float('nan')] * N)
        self.Pressure = numpy.array([float('nan')] * N)
        self.Enthalpy = numpy.array([float('nan')] * N)
        self.MassRate = numpy.array([float('nan')] * N)
        for k in self.ThermoTable.MassFraction.keys():
            self.MassFraction[k] = numpy.array([float('nan')] * N)

        self.Time[0:len(time)] = numpy.array(time)
        self.Pressure[0:len(pressure)] = numpy.array(pressure)
        self.Enthalpy[0:len(enthalpy)] = numpy.array(enthalpy)
        self.MassRate[0:len(mass_rate)] = numpy.array(mass_rate)
        # Mass fractions - constrain to 1.0 as well
        sum = numpy.array([0.0] * N)
        for k in self.ThermoTable.MassFraction.keys():
            if k == self.ThermoTable.mainComponent():
                continue
            try:
                self.MassFraction[k][0:len(mass_fraction[k])] = numpy.array(mass_fraction[k])
            except KeyError:
                pass
            sum += self.MassFraction[k]
        self.MassFraction[self.ThermoTable.mainComponent()] = 1.0 - sum
        
    def addNode(self, parent):
        """
        Return xml node with own data
        """
        node = addNode(parent, self.tag(), attr = {'class': 'WellheadProfileFieldData'})
        addNode(node, 'timefilteron', self.TimeFilterOn)
        addNode(node, 'timefilterinterval', self.TimeFilterInterval)
        addNode(node, 'time', self.Time)
        addNode(node, 'pressure', self.Pressure)        
        addNode(node, 'enthalpy', self.Enthalpy)
        addNode(node, 'massrate', self.MassRate)
        mf = addNode(node, 'massfraction')
        for k in self.MassFraction.keys():
            addNode(mf, ('X' + k).lower(), self.MassFraction[k])
        return node    

    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        self.TimeFilterOn = nodeBool(node, 'timefilteron')
        self.TimeFilterInterval = nodeFloat(node, 'timefilterinterval')
        t = nodeFloatVector(node, 'time')
        p = nodeFloatVector(node, 'pressure')
        h = nodeFloatVector(node, 'enthalpy')
        w = nodeFloatVector(node, 'massrate')
        X = {}
        mf = node.find('massfraction')
        for k in self.ThermoTable.components():
            X[k] = nodeFloatVector(mf, ('X' + k).lower())
        self.setData(time = t, pressure = p, enthalpy = h, mass_rate = w, mass_fraction = X)

class WellheadProfile(object):
    def __init__(self, thermotable):
        """
        Data relating to the wellhead profile
        """
        self.ThermoTable = thermotable
        # create the public structure
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.FieldData = WellheadProfileFieldData(self.ThermoTable)
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'wellheadprofile'

    def addNode(self, parent):
        """
        Return xml node with own data
        """
        node = addNode(parent, self.tag(), attr = {'class': 'WellheadProfile'})
        self.FieldData.addNode(node)
        return node    
        
    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        self.FieldData.parse(node.find(self.FieldData.tag()))
    
class FeedzoneModifier(object):
    def __init__(self):
        """
        Abstract modifier class for feedzones
        Sublasses must implement className() method
        """
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'feedzonemodifier'
        
    def addNode(self, parent):
        """
        Default class implementation
        """
        return addNode(parent, self.tag(), attr = {'class': self.className()})

    def parse(self, node):
        pass
    
class FZM_Constant(FeedzoneModifier):
    def __init__(self):
        super().__init__()
        
    def className(self):
        return "FZM_Constant"
        
class FZM_Chronological(FeedzoneModifier):
    def __init__(self):
        """
        Feedzone modifier using a factor from a chronological table.
        """
        super().__init__()
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.table = {}.copy()
        
    def className(self):
        return "FZM_Chronological"
    
    def add(self, t, f):
        """
        Add a factor entry to the table
        
        t               : time in seconds since 1/1/1970
        f               : factor [1]
        """
        self.table[t] = f
        
    def addNode(self, parent):
        node = super().addNode(parent)
        addTableNode(node, 'factortable', self.table, 'time', 'value')
        return node
    
    def parse(self, node):
        self.table = nodeFloatTable(node, 'factortable', 'time', 'value')

def newFeedzoneModifier(class_t):
    """
    Object factory for creating feedzone modifier of given class_t
    """
    if class_t == 'FZM_Constant':
        return FZM_Constant()
    elif class_t == 'FZM_Chronological':
        return FZM_Chronological()
    raise KeyError("[newFeedzoneModifier]: Unknown class type '%s'." % class_t)

class FeedzoneType(enum.Enum):
    """
    Feedzone type; same values as internally used in Volung's Regin module
    """
    DarcyForchheimer                = 0
    Lumped                          = 1
    Fixed                           = 2
    SimplifiedDarcyForchheimer      = 3
    DarcyForchheimerV2              = 4
    SimplifiedDarcyForchheimerV2    = 5
    
class Feedzone(object):
    def __init__(self, elevation = 0.0, type = FeedzoneType.Lumped, permeability_height = 0.0, productive_index = 0.0, mass_rate = 0.0, radius_of_investigation = 100.0, skin = 0.0, forchheimer_coefficient = 0, thickness = 0.0, n = 1, ecf = False, pcm = False, fz_modifier = FZM_Constant(), color = (255,255,255)):
        """
        Convenience constructor
        
        Some parameters are superfluous, i.e. the type determines which parameters are used and how.
        
        elevation                   : feedzone elevation [m]
        type                        : feedzone type as FeedzoneType enumerator, see manual for details
        permeability_height         : permeability * height [m] (kh)
        productive_index            : productive index, [kg/(s * Pa)]
        mass_rate                   : mass rate [kg/s], +ve for producing, -ve for injecting
        radius_of_investigation     : radius of investigation [m]
        skin                        : skin factor [1]
        forchheimer coefficient     : Forchheimer coefficient; type dependent (see manual)
        thickness                   : feedzone thickness (for n != 1)
        n                           : number of discretized feeds for elongated feedzones
        ecf                         : use enthalpy correction factor (True/False)
        pcm                         : use pressure corrected mobility (True/False)
        fz_modifier                 : feedzone modifer instance
        color                       : RGB color information
        """
        self.Elevation = elevation
        self.Thickness = thickness
        self.N = n
        self.Type = type
        self.RadiusOfInvestigation = radius_of_investigation
        self.Skin = skin
        self.ForchheimerCoefficient = forchheimer_coefficient
        self.PermeabilityHeight = permeability_height
        self.ProductiveIndex = productive_index
        self.MassRate = mass_rate
        self.Color = color
        self.EnthalpyCorrectionFactor = ecf
        self.PressureCorrectedEnthalpy = pcm
        self.Modifier = fz_modifier
        
    def clear(self):
        """
        Clears the current data
        """
        self.Elevation = 0.0
        self.Thickness = 0.0
        self.N = 1
        self.Type = FeedzoneType.Fixed
        self.RadiusOfInvestigation = 100.0
        self.Skin = 0.0
        self.ForchheimerCoefficient = 0.0
        self.PermeabilityHeight = 0.0
        self.ProductiveIndex = 0.0
        self.MassRate = 0.0
        self.Color = (255,255,255)
        self.EnthalpyCorrectionFactor = False
        self.PressureCorrectedEnthalpy = False
        self.Modifier = FZM_Constant()

    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'feedzone'

    def addNode(self, parent):
        """
        Return xml node with own data
        """
        node = addNode(parent, self.tag(), attr = {'class': 'Feedzone'})
        addNode(node, 'z', self.Elevation)
        addNode(node, 'thickness', self.Thickness)
        addNode(node, 'n', self.N)
        addNode(node, 'type', self.Type.value)
        addNode(node, 'radiusofinvestigation', self.RadiusOfInvestigation)
        addNode(node, 'skin', self.Skin)
        addNode(node, 'forchheimercoefficient', self.ForchheimerCoefficient)
        addNode(node, 'permeabilityheight', self.PermeabilityHeight)
        addNode(node, 'productiveindex', self.ProductiveIndex)
        addNode(node, 'massrate', self.MassRate)
        addColorNode(node, 'color', self.Color)
        addNode(node, 'useenthalpycorrectionfactor', self.EnthalpyCorrectionFactor)
        addNode(node, 'usepressurecorrectedmobility', self.PressureCorrectedEnthalpy)
        self.Modifier.addNode(node)
        return node            
    
    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        self.Elevation = nodeFloat(node, 'z')
        self.Thickness = nodeFloat(node, 'thickness')
        self.N = nodeInt(node, 'n')
        self.Type = FeedzoneType(nodeInt(node, 'type'))
        self.RadiusOfInvestigation = nodeFloat(node, 'radiusofinvestigation')
        self.Skin = nodeFloat(node, 'skin')
        self.ForchheimerCoefficient = nodeFloat(node, 'forchheimercoefficient')
        self.PermeabilityHeight = nodeFloat(node, 'permeabilityheight')
        self.ProductiveIndex = nodeFloat(node, 'productiveindex')
        self.MassRate = nodeFloat(node, 'massrate')
        self.Color = nodeColor(node, 'color')
        self.EnthalpyCorrectionFactor = nodeBool(node, 'useenthalpycorrectionfactor')
        self.PressureCorrectedEnthalpy = nodeBool(node, 'usepressurecorrectedmobility')
        self.Modifier = newFeedzoneModifier(node.find(FeedzoneModifier.tag()).attrib['class'])
        self.Modifier.parse(node.find(self.Modifier.tag()))
        
class FeedzoneCollection(object):
    def __init__(self):
        """
        Collection holding the feedzones
        """
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.Zones = []
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'feedzonecollection'
        
    def add(self, fz):
        """
        Add a feedzone to the collection.
        """
        self.Zones.append(fz)
        
    def elevations(self):
        """
        Returns the feedzone elevations in sorted (ascending) order as a list
        """
        l = []
        for d in self.Zones:
            l.append(d.Elevation)
        return sorted(l)
    
    def lowestElevation(self):
        """
        Returns the elevation of the lowest feedzone - often required for upwards integrations
        """
        return self.elevations()[0]
        
    def addNode(self, parent):
        """
        Return xml node with own data
        """
        node = addNode(parent, self.tag(), attr = {'class': 'FeedzoneCollection'})
        for z in self.Zones:
            z.addNode(node)
        return node    
    
    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        for fzn in node.iter(Feedzone.tag()):
            fz = Feedzone()
            fz.parse(fzn)
            self.add(fz)
    
class WellboreSimulationType(enum.Enum):
    """
    Enumerator for wellbore simulation types; values are same as internally used in Volsung's Regin module
    """
    Upwards                     = 0
    Downwards                   = 1
    DC_BruteForce               = 2
    DC_FullCurve                = 3
    DC_MassRateSearch           = 4
    DC_WellheadPressureSearch   = 5
    IC_BruteForce               = 6
    IC_FullCurve                = 7
    IC_MassRateSearch           = 8
    IC_WellheadPressureSearch   = 9
    DetermineBottomPIBasic      = 10
    DetermineBottomPIFull       = 11
    
class WellboreSimulation(object):
    def __init__(self, thermotable):
        """
        This class holds the parameters which are required to perform a simulation on a wellbore model.
        """
        self.ThermoTable = thermotable
        self.clear()
        
    def clear(self):
        """
        Clear the current data
        """
        # Initial condition for Upwards/Downwards simulations
        self.InitialCondition = FlowState(self.ThermoTable, w = 0, p = 0, h = 0, q = 0, X = {})
        self.InitialCondition.constrain()
        self.Type = WellboreSimulationType.Upwards
        self.InitialElevation = 0.0
        # targets/tolerances for searches, e.g. deliverability or injectivity curves
        self.MassRateTarget = 0.0
        self.MassRateTolerance = 0.1
        self.PressureTarget = 0.0
        self.PressureTolerance = 0.1e+5
        self.MinimumMassRate = 0.0
        # auxiliary parameters
        self.DeltaBottomHolePressure = 1.0e5
        self.MinimumWellheadPressure = 1.0e5
        self.MaximumWellheadPressure = 10.0e5
        self.MassRateResolution = 0.02
        self.PressureResolution = 1e5
        self.FeedzoneEliminationThreshold = 0.05
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'wellboresimulation'
        
    def addNode(self, parent):
        """
        Return xml node with own data
        """
        node = addNode(parent, self.tag(), attr = {'class': 'WellboreSimulation'})
        addNode(node, 'simulationtype', self.Type.value)
        # initial conditions
        self.InitialCondition.constrain()           # ensure mass fractions add up to 1.0
        incon = addNode(node, 'initialconditions')
        addNode(incon, 'w', self.InitialCondition.MassRate)
        addNode(incon, 'q', self.InitialCondition.EnergyRate)
        addNode(incon, 'p', self.InitialCondition.Pressure)
        addNode(incon, 'h', self.InitialCondition.Enthalpy)
        for k in self.InitialCondition.MassFraction.keys():
            addNode(incon, ('X' + k).lower(), self.InitialCondition.MassFraction[k])
        # initial elevation
        addNode(node, 'initialelevation', self.InitialElevation)
        # targets/tolerances
        addNode(node, 'massratetarget', self.MassRateTarget)        
        addNode(node, 'massratetolerance', self.MassRateTolerance)
        addNode(node, 'pressuretarget', self.PressureTarget)
        addNode(node, 'pressuretolerance', self.PressureTolerance)
        addNode(node, 'minimummassrate', self.MinimumMassRate)
        # auxiliary
        addNode(node, 'deltabottomholepressure', self.DeltaBottomHolePressure)
        addNode(node, 'minimumwellheadpressure', self.MinimumWellheadPressure)
        addNode(node, 'maximumwellheadpressure', self.MaximumWellheadPressure)
        addNode(node, 'massrateresolution', self.MassRateResolution)
        addNode(node, 'pressureresolution', self.PressureResolution)
        addNode(node, 'feedzoneeliminationthreshold', self.FeedzoneEliminationThreshold)
        return node    
    
    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        self.Type = WellboreSimulationType(nodeInt(node, 'simulationtype'))
        # initial conditions
        incon = node.find('initialconditions')
        self.InitialCondition.MassRate = nodeFloat(incon, 'w')
        self.InitialCondition.EnergyRate = nodeFloat(incon, 'q')
        self.InitialCondition.Pressure = nodeFloat(incon, 'p')
        self.InitialCondition.Enthalpy = nodeFloat(incon, 'h')
        self.InitialCondition.MassFraction.clear()
        for k in self.ThermoTable.components():
            self.InitialCondition.MassFraction[k] = nodeFloat(incon, ('X' + k).lower())
        self.InitialCondition.constrain()
        # initial elevation
        self.InitialElevation = nodeFloat(node, 'initialelevation')
        # targets/tolerances
        self.MassRateTarget = nodeFloat(node, 'massratetarget')
        self.MassRateTolerance = nodeFloat(node, 'massratetolerance')
        self.PressureTarget = nodeFloat(node, 'pressuretarget')
        self.PressureTolerance = nodeFloat(node, 'pressuretolerance')
        self.MinimumMassRate = nodeFloat(node, 'minimummassrate')
        self.DeltaBottomHolePressure = nodeFloat(node, 'deltabottomholepressure')
        self.MinimumWellheadPressure = nodeFloat(node, 'minimumwellheadpressure')
        self.MaximumWellheadPressure = nodeFloat(node, 'maximumwellheadpressure')
        self.MassRateResolution = nodeFloat(node, 'massrateresolution')
        self.PressureResolution = nodeFloat(node, 'pressureresolution')
        self.FeedzoneEliminiationThreshold = nodeFloat(node, 'feedzoneeliminationthreshold')
    
class WellboreProfileIntegratorType(enum.Enum):
    """
    Type for integrating the wellbore profile.
    """    
    Euler       = "PODEI_Euler"
    RungeKutta4 = "PODEI_RungeKutta4"
    
class PressureDropCorrelation(enum.Enum):
    """
    Type for pressure drop correlations
    """    
    Homogeneous         = "PD_Homogeneous"
    DunsRos             = "PD_DunsRos"
    HasanKabir2010      = "PD_HasanKabir2010"
    HagedornBrown       = "PD_HagedornBrown"
    ChisholmOrkiszewski = "PD_ChisholmOrkiszewski"
    ChisholmArmand      = "PD_ChisholmArmand"
    Orkiszewski         = "PD_Orkiszewski"
    LockhartMartinelli  = "PD_LockhartMartinelli"
    Hadgu               = "PD_Hadgu"
    Garg                = "PD_Garg"
    
class WellboreModelType(enum.Enum):
    """
    Type for wellbore models
    """
    Unspecified         = 0
    Injector            = 1
    Producer            = 2

class WellboreModel(object):
    def __init__(self, thermotable):
        """
        The WellboreModel class is the main structure containing the wellbore model parameters
        """
        self.ThermoTable = thermotable
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        # create the public structure
        self.CommissionTime = 0.0                               # commission time - required for heat loss calculations
        self.WellTrack = WellTrack()
        self.FlowPath = FlowPath()
        self.HeatLossParameters = HeatLossParameters()
        self.WellboreProfile = WellboreProfile()
        self.WellheadProfile = WellheadProfile(self.ThermoTable)
        self.Integrator = WellboreProfileIntegratorType.Euler
        self.PressureDropCorrelation = PressureDropCorrelation.Homogeneous
        self.Type = WellboreModelType.Unspecified
        self.Feedzones = FeedzoneCollection()
        self.Simulation = WellboreSimulation(self.ThermoTable)
        
    def tag(self):
        """
        Returns the xml tag
        """
        return 'wellboremodel'

    def addNode(self, parent):
        """
        Return xml node with own data
        """        
        node = addNode(parent, self.tag(), attr = {'class': 'WellboreModel'})
        addNode(node, 'couplingmode', 0)
        self.WellTrack.addNode(node)
        self.FlowPath.addNode(node)
        self.HeatLossParameters.addNode(node)
        self.WellboreProfile.addNode(node)
        self.WellheadProfile.addNode(node)
        self.Feedzones.addNode(node)
        self.Simulation.addNode(node)
        # the integrator - not strictly a type but a class without parameters
        wbi = addNode(node, 'pipeprofileintegrator', attr = {'class': 'WellboreProfileIntegrator'})
        addNode(wbi, 'pipeodeintegrator', attr = {'class': str(self.Integrator.value)})
        # the pressure drop correlation - not strictly a type but a class without parameters
        addNode(node, 'pressuredropcorrelation', attr = {'class': str(self.PressureDropCorrelation.value)})
        # type of the model - mostly used for GUI purposes, i.e. selecting right choice of available options
        addNode(node, 'wellboremodeltype', self.Type.value)
        return node
    
    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        self.WellTrack.parse(node.find(self.WellTrack.tag()))
        self.FlowPath.parse(node.find(self.FlowPath.tag()))
        self.HeatLossParameters.parse(node.find(self.HeatLossParameters.tag()))
        self.WellboreProfile.parse(node.find(self.WellboreProfile.tag()))
        self.WellheadProfile.parse(node.find(self.WellheadProfile.tag()))
        self.Feedzones.parse(node.find(self.Feedzones.tag()))
        self.Simulation.parse(node.find(self.Simulation.tag()))
        # the integrator - not strictly a type but a class without parameters
        self.Integrator = WellboreProfileIntegratorType(node.find('pipeprofileintegrator').find('pipeodeintegrator').attrib['class'])
        # the pressure drop correlation - not strictly a type but a class without parameters
        self.PressureDropCorrelation = PressureDropCorrelation(node.find('pressuredropcorrelation').attrib['class'])
        # type of the model - mostly used for GUI purposes, i.e. selecting right choice of available options
        self.Type = WellboreModelType(nodeInt(node, 'wellboremodeltype'))
    
class WellboreModelEditor(object):
    def __init__(self):
        """
        Contains some parameters regarding visualization.
        """
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.RadiusScale = 75.0
        self.FeedzoneRadiusScale = 20.0
        self.ClipSliderPosition = 65535
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'wellboremodeleditor'
        
    def addNode(self, parent):
        """
        Return xml node with own data
        """        
        node = addNode(parent, self.tag(), attr = {'class': 'WellBoreModelEditor'})
        addNode(node, 'radiusscale', self.RadiusScale)
        addNode(node, 'feedzoneradiusscale', self.FeedzoneRadiusScale)
        addNode(node, 'clipsliderposition', self.ClipSliderPosition)
        return node
    
    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        self.RadiusScale = nodeFloat(node, 'radiusscale')
        self.FeedzoneRadiusScale = nodeFloat(node, 'feedzoneradiusscale')
        self.ClipSliderPosition = nodeInt(node, 'clipsliderposition')
