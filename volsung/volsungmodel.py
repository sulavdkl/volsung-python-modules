#!/usr/bin/env python3

"""

volsung.py

library to read in Volsung models consisting of a set of 

a) a Sigurd output file (hdf5)
b) a Brynhild input file (xml) [optional]

The model class can be used to create special tasks which need the interaction of the input/output data.

If you don't specify a Brynhild input file then the xml file will be loaded from the xml data stored in the Results.sigurd file instead.

"""

from volsung.brynhild import *
from volsung.sigurd import *
from volsung.reservoir import *
from volsung.flownetwork import *
from volsung.sources import *
import tempfile
import shutil

class VolsungModel(object):
    """
    Volsung model class
    """
    
    def __init__(self, results_fname = "Results.sigurd", brynhild_fname = "", useTempHDF5 = True):
        self.hdfFile = self.__openHDF5(results_fname, useTempHDF5)
        self.reservoir = Reservoir(self.hdfFile)
        self.sources = Sources(self.hdfFile)
        self.flowNetwork = FlowNetwork(self.hdfFile)
        if brynhild_fname != "":
            self.brynhild = Brynhild(brynhild_fname)
        else:
            self.brynhild = Brynhild()
            self.brynhild._setXMLText(self.reservoir.xmlInputText())
            
        # let the objects in the flow network read additional data from brynhild
        for obj in self.flowNetwork.portObjects:
            obj._readXML(self.brynhild.flowNetworkObjectNode(obj.objectId))
            
    def __openHDF5(self, results_fname, useTempHDF5):
        """
        Opens and returns the HDF5 file with the results.
        If useTempHDF5 is True then a temporary file copy of the results file is created.
        This is done to prevent locking the file handle, i.e. running a simulation with a 
        locked file handle will prevent the simulator from saving the results of the current run.
        """
        if not useTempHDF5:
            return h5py.File(results_fname, "r")
        # create a temporary file
        tf = tempfile.NamedTemporaryFile()
        tf.close()
        shutil.copy(results_fname, tf.name)
        return h5py.File(tf.name, "r")
        
    
    def close(self):
        """
        Closes the HDF5 results file.
        If no temporary HDF5 results files are used then this will release the file handle,
        enabling the simulator to again save results.
        Use the close() method meticuluously when done analyzing data to prevent issues, unless
        you open the model with useTempHDF5 set to True.
        """
        self.hdfFile.close()
    