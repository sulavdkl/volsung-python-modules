#!/usr/bin/env python3

"""

yggdrasil_xml.py

Library to provide xml related classes from Yggdrasil

"""

from math import isnan

#
# Thermodynamic auxiliaries
#
class ThermoState(object):
    def __init__(self, thermotable, p = float('nan'), h = float('nan'), X = {}):
        """
        Basic structure containing a pressure, enthalpy, component mass fraction state
        
        Pressure                : pressure [Pa]
        Enthalpy                : specific enthalpy [J/kg]
        MassFraction            : dictionary containing mass fractions, e.g. MassFraction['CO2'] = 0.1

        Note:
            - You can use the thermotable's functionality to determine the enthalpy if you only know pressure/temperature            
            - Once the thermotable is set you can create ThermoState(thermotable) and it will take over the current values for the thermotable
        """
        self.ThermoTable = thermotable
        if isnan(p):
            self.Pressure = thermotable.Pressure
        else:
            self.Pressure = p
        if isnan(h):
            self.Enthalpy = thermotable.Enthalpy
        else:
            self.Enthalpy = h
        self.MassFraction = {}.copy()
        for k in self.ThermoTable.components():
            try:
                self.MassFraction[k] = X[k]
            except KeyError:
                self.MassFraction[k] = 0.0
        
    def constrain(self):
        """
        Constrains the mass fractions so they add up to 1.0
        """
        sum = 0.0
        mainComponent = self.ThermoTable.mainComponent()
        for k in self.MassFraction.keys():
            if k != mainComponent:
                sum += self.MassFraction[k]
        self.MassFraction[mainComponent] = 1.0 - sum
        
class FlowState(ThermoState):
    def __init__(self, thermotable, w = 0, q = 0, p = float('nan'), h = float('nan'), X = {}):
        """
        A flowing state inherits ThermoState but extends it by MassRate, Energyrate
        
        MassRate                : mass rate [kg/s]; sign is context dependent
        EnergyRate              : energy rate [J/s]; sign is context dependent
        """
        super().__init__(thermotable, p, h, X)
        self.MassRate = w
        self.EnergyRate = q
        