#!/usr/bin/env python3

"""
gudrun_xml.py

Library to build gudrun wellbore models programmatically

Note: most of the documentation can be found in regin_xml.py
"""

import numpy
import copy
import enum
import sys
from xml.etree import ElementTree

from volsung.regin_xml import *
from volsung.helpers_xml import *
from volsung.frigg_xml import *
from volsung.yggdrasil_xml import *

class ModelVersionInfo(object):
    def __init__(self):
        """
        Contains model versioning info
        """
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.Title = ""
        self.Description = ""
        
    @classmethod
    def tag(self):
        """
        Returns the xml tag
        """
        return 'modelversioninfo'
        
    def addNode(self, parent):
        """
        Return xml node with own data
        """        
        node = addNode(parent, self.tag(), attr = {'class': 'ModelVersionInfo'})
        addNode(node, 'title', self.Title)
        addNode(node, 'description', self.Description)
        return node    

    def parse(self, node):
        """
        Parse object from node
        """
        self.clear()
        self.Title = nodeString(node, 'title')
        self.Description = nodeString(node, 'description')

#
# The GudrunXML main class for this module
#

class GudrunXML(object):
    def __init__(self, thermotable = TT_IAPWSIF97(), major=1, minor=14, patch=3):
        """
        Gudrun wellbore model
        
        major, minor, patch                     : integers describing version number in which the wellbore model is to be built
        """
        # set the version number
        # not too important at the moment but may be useful in the future if internal object structure changes
        self._major = major
        self._minor = minor
        self._patch = patch
        # create public structure
        self.ThermoTable = thermotable
        self.clear()
        
    def clear(self):
        """
        Clears the current data
        """
        self.Time = 0.0
        self.Reservoir1D = Reservoir1D(self.ThermoTable)
        self.WellboreModel = WellboreModel(self.ThermoTable)        
        self.WellboreModelEditor = WellboreModelEditor()
        self.ModelVersionInfo = ModelVersionInfo()
        
    def xml(self):
        """
        Returns the xml text
        """        
        # root
        root = Element('root')
        root.set('volsung-version', '%d.%d.%d' % (self._major, self._minor, self._patch))
        # main win
        mwin = addNode(root, 'gudrunmainwindow', attr = {'class': 'GudrunMainWindow'})
        # global settings
        addNode(mwin, 'thermodynamictable', attr = {'class' : self.ThermoTable.className(), 'numberofaqueoustracers' : '0'})
        addNode(mwin, 'time', self.Time)
        # the reservoir
        self.Reservoir1D.addNode(mwin)
        # the main wellbore model
        self.WellboreModel.addNode(mwin)
        # the wellbore model editor
        self.WellboreModelEditor.addNode(mwin)
        # model version info
        self.ModelVersionInfo.addNode(mwin)
        # get xml string and set doctype header
        return prettify_xml(root, doctype = 'Gudrun')
        
    def write(self, fname):
        """
        Write wellbore model to xml file.
        """
        # write to file
        f = open(fname, "w")
        f.write(self.xml())
        f.close()
      
    def read(self, fname):
        """
        Reads a Gudrun wellbore model from xml file.
        """
        tree = ElementTree.parse(fname)
        root = tree.getroot()
        # determine the version
        vs = root.get('volsung-version').split('.')
        self._major = int(vs[0])
        self._minor = int(vs[1])
        self._patch = int(vs[2])
        # main window
        mwin = root.find('gudrunmainwindow')
        # determine the thermodynamic table
        self.ThermoTable = newThermodynamicTable(mwin.find('thermodynamictable').attrib['class'])
        # now we can clear/prepare the data
        self.clear()
        # further global settings
        self.Time = nodeFloat(mwin, 'time')
        # parse the objects
        # the reservoir
        self.Reservoir1D.parse(mwin.find(self.Reservoir1D.tag()))
        # the wellbore model
        self.WellboreModel.parse(mwin.find(self.WellboreModel.tag()))
        # the wellbore model editor
        self.WellboreModelEditor.parse(mwin.find(self.WellboreModelEditor.tag()))
        # the version info
        self.ModelVersionInfo.parse(mwin.find(self.ModelVersionInfo.tag()))
        