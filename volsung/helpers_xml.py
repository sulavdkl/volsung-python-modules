#!/usr/bin/env python3

"""

helpers-xml.py

Library with xml helper functions

"""

import numpy
import enum
import copy
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.dom import minidom

#
# XML auxiliaries - maybe move to another module?
#
def prettify_xml(elem, doctype = '', indent = '\t'):
    """
    Return a pretty-printed XML string for the Element.
    If doctype is provided then the xml header will be replaced by DOCTYPE header
    """
    rough_string = tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    s = reparsed.toprettyxml(indent=indent)
    if doctype != '':
        s = s.replace('<?xml version="1.0" ?>', '<!DOCTYPE %s>' % doctype)
    return s

def addNode(parent, tag, val = None, attr = {}):
    """
    Auxiliary - create and return a new node as child of parent node
    with given tag name, value and optional attributes.
    """
    node = SubElement(parent, tag)
    if val is not None:
        if type(val) == bool:
            # use 0/1 instead of False/True
            if val:
                node.text = '1'
            else:
                node.text = '0'
        elif type(val) == list:
            # use <v> tags for each entry
            for l in val:
                addNode(node, 'v', str(l))
        elif type(val) == numpy.ndarray:
            # use <v index="xxx"> tags for each entry
            node.attrib['size'] = str(val.size)
            for i in range(val.size):
                addNode(node, 'v', str(val[i]), attr = {'index': str(i)})
        else:
            # default
            node.text = str(val)
    if len(attr) > 0:
        node.attrib = copy.copy(attr)
    return node

def addColorNode(parent, tag, rgb):
    """
    Auxiliary - create a color node containing RGB information
    """
    node = addNode(parent, tag)
    addNode(node, 'r', rgb[0])
    addNode(node, 'g', rgb[1])
    addNode(node, 'b', rgb[2])
    if len(rgb) == 3:
        addNode(node, 'alpha', '255')
    else:
        addNode(node, 'alpha', rgb[3])
    return node

def nodeColor(parent, tag):
    node = parent.find(tag)
    raw = []
    raw.append(int(node.find('r').text))
    raw.append(int(node.find('g').text))
    raw.append(int(node.find('b').text))
    raw.append(int(node.find('alpha').text))
    return tuple(raw)

def addTableNode(parent, tag, table_dict, key_tag, value_tag):
    """
    Auxiliary - add a table from a dictionary
    """
    node = addNode(parent, tag)
    for k in table_dict:
        addNode(node, value_tag, str(table_dict[k]), attr = {key_tag: str(k)})
    return node

def nodeFloatTable(parent, tag, key_tag, value_tag):
    table = {}.copy()
    for v in parent.find(tag).iter(value_tag):
        k = float(v.attrib[key_tag])
        table[k] = float(v.text)
    return table

def nodeString(parent, tag):
    return parent.find(tag).text

def nodeFloat(parent, tag):
    return float(parent.find(tag).text)

def nodeInt(parent, tag):
    return int(parent.find(tag).text)

def nodeBool(parent, tag):
    return nodeInt(parent, tag) != 0

def nodeFloatList(parent, tag):
    l = []
    for v in parent.find(tag).iter('v'):
        l.append(float(v.text))
    return l

def nodeFloatVector(parent, tag):
    node = parent.find(tag)
    n = int(node.attrib['size'])
    x = numpy.array([float('nan')] * n, dtype = numpy.float64)
    for v in node.iter('v'):
        index = int(v.attrib['index'])
        x[index] = float(v.text)
    return x

