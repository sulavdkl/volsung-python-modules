# Volsung Python Modules

The Volsung Python Modules supplement the Volsung Geothermal Simulation Package. They provide access to complex data structures and perform pre- and post-processing tasks.

## Dependencies

The modules depend on some other publicly available Python libraries. These can be easily installed using **pip3**:

>$ pip3 install numpy h5py vtk matplotlib xlrd pandas scipy

## Installation

We recommend that you download these as .zip or .tar.gz or set up a clone repository on your computer.

We recommend that you include the location of module package in your **PYTHONPATH** environmental variable - this makes updating of the package much easier. Alternatively you can install the package using

>$ python3 setup.py install

Some of the modules require knowledge about where Volsung is installed on your computer. You will hence need to create the **VOLSUNGPATH** environment variable and set it to the installation folder of Volsung. Under linux you would do this by adding

>export VOLSUNGPATH=/path/to/volsung

into your *.profile* file. You can do the same for the **PYTHONPATH** environmental variable.

If you are using Windows please have a look at [these instructions](https://docs.oracle.com/en/database/oracle/machine-learning/oml4r/1.5.1/oread/creating-and-modifying-environment-variables-on-windows.html#GUID-DD6F9982-60D5-48F6-8270-A27EC53807D0) on how to create and set environment variables.

## Volsung

You will need a running version of Volsung which you can obtain from [Flow State Solutions Ltd](https://www.flowstatesolutions.co.nz).

Note there is an example collection of geothermal models available, some of which demonstrate the use of the Python modules. You can access the Volsung examples [here](https://gitlab.com/drpeterfranz/volsung-examples).

## Contact

If encounter issues or have further questions please contact us at [info@FlowStateSolutions.co.nz](mailto:info@FlowStateSolutions.co.nz).
