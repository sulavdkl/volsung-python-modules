#!/usr/bin/env python3

from distutils.core import setup

setup(name='volsung-python-modules',
      packages = ['volsung'],
      scripts = ['scripts/sigurdcmd.py'],
      )
